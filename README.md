# xlunch
Graphical app launcher for X, using pure Xlib and Imlib2.

## Fork for Puppy Linux

This is intended to work in Puppy Linux, and is a **work-in-progress**

### Changes: 

1. CFLAGS in Makefile added `-std=c99`
2. In xlunch.c:  
    a. set color depth to 24 (not 32)  
    b. very bad hack to disabled App name truncation  
3. re-write parts of `extra/genentries`:  
    a. look in more dirs for the icons  
    b. better SVG to PNG conversion  
    c. clean ups and fixes  
4. added `extra/xlunchr` (installs to /usr/bin/xlunchr):  
    a. based on https://github.com/saymoncoppi/xlunchr  
    b. simplified (removed custom config stuff, use `genentries` script instead)  
    c. added separate `--desktop` mode, which uses `--desktop --dontquit`  
  

# website

detailed info at: http://www.xlunch.org/

# compile and test with:

```
make test
```

# screenshot

![Screenshot](https://gitlab.com/sc0ttj/xlunch/raw/master/extra/screenshot.png "Screenshot")
